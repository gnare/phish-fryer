package com.voxelbuster.phishfryer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
@Setter
public class FryerConfig implements Serializable {
    private String endpointUrl = "https://uspdemivtow.info/usps/addres";
    private String method = "POST";

    private ArrayList<String> userAgents;
    private ArrayList<String> firstNames;
    private ArrayList<String> lastNames;
    private ArrayList<String> emails;

    public FryerConfig() {
        BufferedReader fr = new BufferedReader(new InputStreamReader(
                Objects.requireNonNull(getClass().getResourceAsStream("/userAgents.txt"))));
        userAgents = (ArrayList<String>) fr.lines().collect(Collectors.toList());

        fr = new BufferedReader(new InputStreamReader(
                Objects.requireNonNull(getClass().getResourceAsStream("/firstNames.txt"))));
        firstNames = (ArrayList<String>) fr.lines().collect(Collectors.toList());

        fr = new BufferedReader(new InputStreamReader(
                Objects.requireNonNull(getClass().getResourceAsStream("/lastNames.txt"))));
        lastNames = (ArrayList<String>) fr.lines().collect(Collectors.toList());

        fr = new BufferedReader(new InputStreamReader(
                Objects.requireNonNull(getClass().getResourceAsStream("/emails.txt"))));
        emails = (ArrayList<String>) fr.lines().collect(Collectors.toList());
    }
}
